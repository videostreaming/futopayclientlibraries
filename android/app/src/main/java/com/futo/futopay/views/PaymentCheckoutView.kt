package com.futo.futopay.views

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.Animatable
import android.icu.text.NumberFormat
import android.icu.util.Currency
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.futo.futopay.PaymentBreakdown
import com.futo.futopay.PaymentConfigurations
import com.futo.futopay.R
import com.futo.futopay.formatMoney
import java.util.Locale
import kotlin.math.pow

class PaymentCheckoutView : ConstraintLayout {
    private var _isValid = false;

    private val buttonPay: FrameLayout
    private val textMethod: TextView
    private val textCurrency: TextView
    private val textPostalCode: TextView
    private val textProduct: TextView
    private val textProductPrice: TextView
    private val textTaxPercentage: TextView
    private val textTax: TextView
    private val textTotal: TextView
    private val textCountry: TextView
    private val editEmail: EditText
    private val editEmailConfirm: EditText
    private val layoutBreakdown: LinearLayout
    private val layoutLoader: FrameLayout
    private val imageLoader: ImageView
    private val textError: TextView
    private val textEmailSubtext: TextView
    private val textPostalCodeHeader: TextView
    private val buttonChangePostalCode: FrameLayout
    private val country: PaymentConfigurations.CountryDescriptor

    constructor(context: Context, method: String, currency: PaymentConfigurations.CurrencyDescriptor, country: PaymentConfigurations.CountryDescriptor, postalCode: String?, onBuy: (String)->Unit, onChangeCountry: ()->Unit, onChangeCurrency: ()->Unit, onChangePostalCode: ()->Unit): super(context) {
        inflate(context, R.layout.payment_checkout, this);

        buttonPay = findViewById(R.id.button_pay);
        textMethod = findViewById(R.id.text_method)
        textCurrency = findViewById(R.id.text_currency)
        textPostalCode = findViewById(R.id.text_postal_code)
        textPostalCodeHeader = findViewById(R.id.text_postal_code_header)
        buttonChangePostalCode = findViewById(R.id.button_change_postal_code)
        textProduct = findViewById(R.id.text_product)
        textProductPrice = findViewById(R.id.text_product_price)
        textTaxPercentage = findViewById(R.id.text_tax_percentage)
        textTax = findViewById(R.id.text_tax)
        textTotal = findViewById(R.id.text_total)
        textCountry = findViewById(R.id.text_country)
        editEmail = findViewById(R.id.edit_email)
        editEmailConfirm = findViewById(R.id.edit_email_confirm)
        layoutBreakdown = findViewById(R.id.layout_breakdown)
        layoutLoader = findViewById(R.id.layout_loader)
        textError = findViewById(R.id.text_error)
        imageLoader = findViewById(R.id.image_loader)
        textEmailSubtext = findViewById(R.id.text_email_subtext)
        this.country = country

        //findViewById<ImageView>(R.id.image_method).setImageResource(paymentMethod.image);
        textMethod.text = method.replaceFirstChar { it.uppercase() }
        textCountry.text = country.nameEnglish;
        textCurrency.text = currency.id.uppercase();

        findViewById<FrameLayout>(R.id.button_change_country).setOnClickListener {
            onChangeCountry();
        };

        findViewById<FrameLayout>(R.id.button_change_currency).setOnClickListener {
            onChangeCurrency();
        };

        buttonChangePostalCode.setOnClickListener {
            onChangePostalCode();
        };

        editEmail.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) = Unit
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                validateInput()
            }
        });
        editEmailConfirm.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) = Unit
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                validateInput()
            }
        });

        buttonPay.alpha = 0.4f;
        buttonPay.setOnClickListener {
            if (!_isValid) {
                return@setOnClickListener;
            }

            onBuy(editEmail.text.toString());
        }

        setPostalCode(country.id, postalCode);
        setPaymentBreakdown(null);
    }

    private fun validateInput() {
        val emailRegex = """(?:[a-zA-Z0-9!#${'$'}%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#${'$'}%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])""".toRegex()

        val text = editEmail.text.toString()
        if (!emailRegex.matches(text)) {
            _isValid = false
            textEmailSubtext.setTextColor(Color.rgb(0xFF, 0x00, 0x00))
            textEmailSubtext.text = context.getString(R.string.email_is_invalid)
            buttonPay.alpha = 0.4f;
            return
        }

        if (editEmail.text.toString() != editEmailConfirm.text.toString()) {
            _isValid = false
            textEmailSubtext.setTextColor(Color.rgb(0xFF, 0x00, 0x00))
            textEmailSubtext.text = context.getString(R.string.email_must_match)
            buttonPay.alpha = 0.4f;
            return
        }

        textEmailSubtext.setTextColor(Color.rgb(0x99, 0x99, 0x99));
        textEmailSubtext.text = context.getString(R.string.required_to_accurately_calculate_the_applicable_sales_tax);
        buttonPay.alpha = 1.0f;
        _isValid = true
    }

    private fun setPostalCode(countryId: String, postalCode: String? = null) {
        if (countryId.equals("us", ignoreCase = true) || countryId.equals("ca", ignoreCase = true)) {
            textPostalCode.visibility = View.VISIBLE
            textPostalCodeHeader.visibility = View.VISIBLE
            buttonChangePostalCode.visibility = View.VISIBLE

            if (postalCode != null) {
                textPostalCode.text = postalCode.uppercase();
                textPostalCode.setTextColor(Color.rgb(0xFF, 0xFF, 0xFF))
            } else {
                textPostalCode.text =context.getString(R.string.missing);
                textPostalCode.setTextColor(Color.rgb(0xFF, 0, 0))
            }
        } else {
            textPostalCode.visibility = View.GONE
            textPostalCodeHeader.visibility = View.GONE
            buttonChangePostalCode.visibility = View.GONE
        }
    }

    fun setPaymentBreakdown(paymentBreakdown: PaymentBreakdown?) {
        if (paymentBreakdown == null) {
            textError.visibility = View.GONE
            layoutBreakdown.visibility = View.GONE
            layoutLoader.visibility = View.VISIBLE
            (imageLoader.drawable as Animatable?)?.start()
            return;
        }

        (imageLoader.drawable as Animatable?)?.stop()
        layoutLoader.visibility = View.GONE
        textError.visibility = View.GONE
        layoutBreakdown.visibility = View.VISIBLE
        textProduct.text = paymentBreakdown.productName;
        textProductPrice.text = formatMoney(country.id, paymentBreakdown.currency, paymentBreakdown.productPrice)
        textTaxPercentage.text = "%.2f".format(paymentBreakdown.taxPercentage);
        textTax.text = formatMoney(country.id, paymentBreakdown.currency, paymentBreakdown.taxPrice)
        textTotal.text = formatMoney(country.id, paymentBreakdown.currency, paymentBreakdown.totalPrice)
        textCurrency.text = paymentBreakdown.currency.uppercase()
    }

    fun setError(error: String) {
        editEmail.text.clear()
        buttonPay.alpha = 0.4f
        (imageLoader.drawable as Animatable?)?.stop()
        layoutLoader.visibility = View.GONE
        layoutBreakdown.visibility = View.GONE
        textError.visibility = View.VISIBLE
        textError.text = error
        editEmail.visibility = View.INVISIBLE
        textEmailSubtext.visibility = View.INVISIBLE
    }
}